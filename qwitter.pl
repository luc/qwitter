#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use warnings;
use strict;
use 5.10.0;

use utf8;
use Data::Dumper;
use AnyEvent;
use AnyEvent::Twitter;
use Mojo::Util qw<slurp spurt>;

# Go to https://apps.twitter.com/app/new to get the keys and secrets
my $t = AnyEvent::Twitter->new(
    consumer_key    => '',
    consumer_secret => '',
    token           => '',
    token_secret    => '',
);

# Every URL will count for 21 characters.
# The phrase will be prepend by the twitter name (@someone) of the person which mentionned you,
# so be succinct to make sure you won't go over 140 chars in total.
my $phrase = 'Bonjour, j\'arrête Twitter fin octobre. Retrouvez-moi sur #Diaspora :-) https://framasphere.org/people/b13eb6b0beac0131e7e32a0000053625 #ByeTwitter';

my $last_id = 1;
if (-e 'last_id.txt') {
    $last_id = slurp('last_id.txt') || 1;
}
say "Last id: $last_id";

while (1) {
    my $cv = AE::cv;
    $cv->begin;
    $t->get('https://api.twitter.com/1.1/statuses/mentions_timeline.json', 
        { since_id => $last_id },
        sub {
            my ($header, $response, $reason) = @_;
            say scalar($reason);
            for my $tweet (@{$response}) {
                say $tweet->{text};
                say '@'.$tweet->{user}{screen_name}.' '.$phrase."\n";
                $t->post('statuses/update',
                    {
                        status                => '@'.$tweet->{user}{screen_name}.' '.$phrase,
                        in_reply_to_status_id => $tweet->{id}
                    },
                    sub {
                        my ($header, $response, $reason) = @_;
                    }
                );
            }

            # Store last responded tweet id
            $last_id = $response->[0]->{id} if defined $response->[0]->{id};
            spurt $last_id, 'last_id.txt';
            say "Last id: $last_id";
            $cv->end;
        }
    );

    $cv->recv;

    # Can't use that API more than 15 times in 15 minutes.
    sleep 60+rand(60);
}
