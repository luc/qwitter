# Qwitter

This is a really simple bot which responds to twitters mentions.

I use it to warn people that I'll soon quit twitter, so… qwitter is its name.

## How to use

### Install

```
git clone https://framagit.org/luc/qwitter.git
cd qwitter
sudo cpan Carton
carton install
```

(you may need `build-essential` and `libssl-dev` to run `carton install` successfully)

### Configure

Then go to https://apps.twitter.com/app/new, register a new app, add it to your account and grab the keys and secrets.

Edit `qwitter.pl` to add the keys and secrets and to change the answer that your bot will post.

### Launch

```
carton exec ./qwitter.pl
```

## Warning

I made this bot quick and dirty.

## License

Released under the terms of the WTFPL, see [LICENSE](LICENSE) file for details.
